const fs = require('fs')
const getLogger = require('webpack-log')
const log = getLogger({
    level: 'debug',
    name: 'spc_log.log',
    timestamp: true,
    unique: false
 })
log.info('Jingle Bells')


module.exports = {
  publicPath: './',
  devServer: {
    disableHostCheck: true,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Request-Headers': 'X-Requested-With, accept, content-type'
      },
    port: 5007,
    host: '0.0.0.0',
    https: {
       key: fs.readFileSync('./privkey1.pem'),
       cert: fs.readFileSync('./fullchain1.pem'),
        },
    public: 'https://westteam.nulstar.com'
  },
  transpileDependencies: [
    'vuetify'
  ],
}


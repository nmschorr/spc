
import axios from 'axios'
import { cobj } from '@/constants/dataConstants.js'
import https from 'https'

const chainid = cobj.chainid
const contaddy = cobj.contaddy
const hrk='https://westteam.nulstar.com:3002/'
//const hrk='https://cors-anywhere.herokuapp.com/'
const url3 = 'https://westteam.nulstar.com:3002/http://westteam.nulstar.com:8003'
const url4 = hrk + cobj.url4

export async function axiosGetReviewsMain(chainid, contaddy, productId, placeholder) {
  const returntype = '(String productId) return Ljava/util/List;'
  const lastlistw = [productId]
  const reqtype = 'getReviews'
  const getReviewsParams = [chainid, contaddy, reqtype, returntype, lastlistw]
  console.log('in queries.axiosGetReviewsMain')
  console.log('c: ' + chainid + ' contaddy: ' + contaddy + ' reqtype: ' + reqtype + ' prodId: ' + productId)

  try {
    var axresultg
    axresultg = await axios.post(url3, {
      method: 'invokeView',
      "jsonrpc": "2.0",
      id: '900092',
      params: getReviewsParams,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      }
    })
  } catch (e) {
    console.log(e)
  }
  console.log('done queries.axiosGetReviewsMain. axresultg.data.result.result: ' + axresultg.data.result.result)
  console.log('axresultg returning: ' + axresultg)
  return axresultg
}

export async function axiosGetProducts(chainid, contaddy, nonehere) {
  console.log('here now in queries axiosGetProducts')
  const reqtype = 'getAllProductIds'
  const returntype = '() return String'
  const lastlist = []
  const getProdsParams = [chainid, contaddy, reqtype, returntype, lastlist]
 
  try {
    var axresult
    console.log('axios getProdsParams: ' + getProdsParams)
    axresult = await axios.post(url3, {
      method: 'invokeView',
      "jsonrpc": "2.0",
      id: 99099,
      params: getProdsParams,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      }
    })
  } catch (e) {
    console.log(e)
  }
  var thisproducts
  thisproducts = JSON.parse(axresult.data.result.result)
  console.log('done in axiosGetProducts. thisproducts: ' + thisproducts)
  // this.cardkey += 1;
  return thisproducts
}

async function axiosGetContracts() {
  var productId = this.prodchoice
  const invokemethod = 'invokeView'
  const returntype = '(String productId) return Ljava/util/List;'
  const lastlistp = [productId]
  const queryid = 900097

  const reqtype = 'getAccountContractList'
  const vparams = [this.chainid, this.contractaddy, reqtype, returntype, lastlistp]
  console.log('just made new axios object in axiosGetContracts')

  try {
    var axresult
    console.log('axiosGetContracts vparams: ' + vparams)
    axresult = await axios.post(url3, {
      method: invokemethod,
      "jsonrpc": "2.0",
      id: queryid,
      params: vparams,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      }
    })
  } catch (e) {
    console.log(e)
  }
  console.log('done in axiosGetContracts')

  this.reviews = JSON.parse(axresult.data.result.result)
  console.log('this.reviews: ' + this.reviews)
  this.cardkey += 1
}

export async function writeReview(writeproduct, wreview) {
  const passwd = cobj.passwd
  const sender = cobj.sender
  const valueasset = cobj.valueasset
  const gasprice = cobj.gasprice
  const gaslimit = cobj.gaslimit

  const args = [writeproduct, wreview]
  const contract_methodname = 'writeReview'
  const invokemethod = 'contractCall'
  const remark = 'call contract'
  const contractdesc = '(String productId, String reviewComments) return LReviewContract$Review;'
  const vparams = [chainid, sender, passwd, valueasset, gaslimit, gasprice,
    contaddy, contract_methodname, contractdesc, args, remark]
  console.log('gaslimit: ' + gaslimit + ' valueasset: ' + valueasset + ' sender: ' + sender)
  console.log('axiosGetContracts vparams: ' + vparams)

  try {
    var axResWrite
    axResWrite = await axios.post(url4, {
      "jsonrpc": "2.0",
      method: invokemethod,
      id: 900099,
      params: vparams,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      }
    })
  } catch (e) { console.log(e) }
  return axResWrite
}

export const MyQueries = {
  axiosGetContracts,
}
